package com.example.goschool;

public class users {

    private String user_id, user_name;

    public users() {
    }

    public users(String user_id, String user_name) {
        this.user_id = user_id;
        this.user_name = user_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }



    /*
    private String id;
    private String name;

    public users() {
    }

    public users(String id) {
        this.id = id;
    }

    public users(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public static void clear() {
    }

    public static void add(users user) {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }*/
}
