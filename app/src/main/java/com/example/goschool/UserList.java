package com.example.goschool;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import java.util.List;

public class UserList extends ArrayAdapter<users> {

    private Activity context;
    private List<users> usersList;
    DatabaseReference databaseReference;
    EditText edtName;


    public UserList(Activity context, List<users> users, DatabaseReference databaseReference, EditText edtName) {
        super(context, R.layout.layout_users_list, users);
        this.context = context;
        this.usersList = users;
        this.databaseReference = databaseReference;
        this.edtName = edtName;

    }

    public View getView(int pos, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_users_list,null,true);

        TextView txtName = (TextView) listViewItem.findViewById(R.id.txtName);
        Button btnDelete = (Button) listViewItem.findViewById(R.id.btnDelete);
        Button btnUpdate =(Button) listViewItem.findViewById(R.id.btnUpdate);

        final users user = usersList.get(pos);
        txtName.setText(user.getUser_name());

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.child(user.getUser_id()).removeValue();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtName.setText(user.getUser_name());
                MainActivity.userId = user.getUser_id();

            }
        });
        return listViewItem;

    }
}

