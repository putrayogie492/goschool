package com.example.goschool;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btnSave;
    EditText edtName;
    DatabaseReference databaseReference;
    ListView listViewusers;
    List<users> User;
    public static String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        User = new ArrayList<users>();

        databaseReference = FirebaseDatabase.getInstance().getReference("users");
        btnSave = (Button) findViewById(R.id.btnsave);
        edtName = (EditText) findViewById(R.id.editName);
        listViewusers = (ListView) findViewById(R.id.listViewUsers);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edtName.getText().toString();
                if(TextUtils.isEmpty(userId)){
                    String id = databaseReference.push().getKey();
                    users users = new users(id, name);
                    databaseReference.child(id).setValue(users);

                    Toast.makeText(MainActivity.this, "User Created Succes", Toast.LENGTH_SHORT).show();
                }else{
                    databaseReference.child(userId).child("name").setValue(name);
                    Toast.makeText(MainActivity.this,"User Update Succes",Toast.LENGTH_SHORT).show();;
                }
                edtName.setText(null);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot) {
                User.clear();
                for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){
                    users user= postSnapshot.getValue(users.class);
                    User.add(user);
                }

                UserList userAdapter = new UserList(MainActivity.this,User,databaseReference,edtName);
                listViewusers.setAdapter(userAdapter);


            }

            @Override
            public void onCancelled( DatabaseError databaseError) {

            }
        });


        }
    }

